using System.Collections.Generic;
using Axlebolt.BoltDatabaseAPI.EditorActions;
using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorElements
{
    internal class DatabaseInfoPanelView : PanelViewBase
    {
        public new class UxmlFactory : UxmlFactory<DatabaseInfoPanelView, UxmlTraits>
        {}

        private ScrollView _databasesView;
        private DatabaseEditView _editView;

        private BoltDatabaseProviderBase _databaseProvider;
        private List<BoltDatabaseEditorActionBase> _actions;

        public override void Init(AttachToPanelEvent evt)
        {
            _databasesView = this.Q<ScrollView>("database-info-view");
            _editView = this.Q<DatabaseEditView>();
            _editView.makeItem = () => new Label();
            _editView.bindItem = (element, id) =>
            {
                var label = element as Label;
                label!.text = _actions[id].DisplayName;
            };
            _editView.onItemsChosen += selectedActions =>
            {
                foreach (BoltDatabaseEditorActionBase action in selectedActions)
                {
                    action.Execute(_databaseProvider);
                }
            };
        }

        public void AddDatabase(string databaseName, List<string> collectionNames)
        {
            var databaseView = new DatabaseView(databaseName, collectionNames);
            _databasesView.Add(databaseView);
        }

        public void SetupActions(BoltDatabaseProviderBase provider, List<BoltDatabaseEditorActionBase> actions)
        {
            _databaseProvider = provider;
            _actions = actions;

            _editView.itemsSource = _actions;
            _editView.Rebuild();
        }
    }

    internal class DatabaseView : Foldout
    {
        public DatabaseView(string databaseName, List<string> collectionNames)
        {
            text = databaseName;
            
            if (collectionNames == null)
                return;

            foreach (var collectionName in collectionNames)
            {
                Add(new DatabaseCollectionView(collectionName));
            }
        }
    }

    internal class DatabaseCollectionView : BindableElement
    {
        public DatabaseCollectionView(string collectionName)
        {
            var nameLabel = new Label(collectionName);
            Add(nameLabel);
        }
    }
}
