using System;
using System.Collections.Generic;
using Axlebolt.BoltDatabaseAPI.EditorControllers;
using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorElements
{
    internal class InspectorView : BindableElement
    {
        public new class UxmlFactory : UxmlFactory<InspectorView, UxmlTraits>
        {}

        private readonly Dictionary<Type, PanelViewBase> _typeToPanel;

        private PanelViewBase _activePanel;

        public InspectorView()
        {
            _typeToPanel = new Dictionary<Type, PanelViewBase>();
            RegisterCallback<AttachToPanelEvent>(Init);
        }

        public void OpenNew()
        {
            OpenPanel<NewPagePanelView>();
        }

        public T OpenPanel<T>() where T : PanelViewBase
        {
            _activePanel?.Show(false);

            var panelView = GetPanel<T>();
            _activePanel = panelView;
            _activePanel.Show(true);

            return panelView;
        }

        public void Show(bool value)
        {
            style.display = new StyleEnum<DisplayStyle>(value ? DisplayStyle.Flex : DisplayStyle.None);
        }

        public T GetPanel<T>() where T : PanelViewBase
        {
            if (!_typeToPanel.TryGetValue(typeof(T), out var panelView))
                throw new Exception($"Panel of type {typeof(T).Name} is not present");

            return panelView as T;
        }

        private void Init(AttachToPanelEvent evt)
        {
            var panelsContainer = this.Q("panels-container");

            var panels = this.Query<PanelViewBase>().ToList();
            foreach (var panelView in panels)
            {
                panelView.Init(evt);
                panelView.Show(false);
                
                if (_typeToPanel.TryAdd(panelView.GetType(), panelView))
                    Add(panelView);
            }

            if (userData is not DatabaseTabController tabController)
                return;
            
            tabController.OnInspectorViewInitialized();
        }
    }
}
