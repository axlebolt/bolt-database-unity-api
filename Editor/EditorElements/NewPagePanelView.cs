using System;
using System.Collections.Generic;
using Axlebolt.BoltDatabaseAPI.Data;
using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorElements
{
    internal class NewPagePanelView : PanelViewBase
    {
        public new class UxmlFactory : UxmlFactory<NewPagePanelView, UxmlTraits>
        {}

        public Action<BoltDatabaseInfo> DatabaseSelectedCallback;

        private ListView _databasesView;
        private List<BoltDatabaseInfo> _databaseInfos;
        
        public override void Init(AttachToPanelEvent evt)
        {
            _databasesView = this.Q<ListView>();

            _databasesView.makeItem = () => new BoltDatabaseInfoView();
            _databasesView.bindItem = (element, id) =>
            {
                var view = element as BoltDatabaseInfoView;
                var databaseInfo = _databaseInfos[id];
                view!.Setup(databaseInfo);
            };

            _databasesView.onItemsChosen += OnDatabaseSelected;
        }

        private void OnDatabaseSelected(IEnumerable<object> selected)
        {
            foreach (var databaseInfo in selected)
            {
                DatabaseSelectedCallback?.Invoke((BoltDatabaseInfo)databaseInfo);
            }
        }

        public void SetBindingData(List<BoltDatabaseInfo> databaseInfos)
        {
            _databaseInfos = databaseInfos;
            _databasesView.itemsSource = _databaseInfos;
            _databasesView.Rebuild();
        }
        
        private class BoltDatabaseInfoView : VisualElement
        {
            private const string MainClassName = "bolt-database-info-view";
            
            private readonly Label _databaseNameLabel;
            private readonly Label _databaseTypeLabel;
            private readonly Label _connectionUriLabel;
            
            public BoltDatabaseInfoView()
            {
                AddToClassList(MainClassName);

                var headerBlock = new VisualElement { name = "header-block" };
                headerBlock.AddToClassList($"{MainClassName}__header");
                Add(headerBlock);
                
                _databaseNameLabel = new Label("DatabaseName");
                _databaseNameLabel.AddToClassList($"{MainClassName}__name-label");
                headerBlock.Add(_databaseNameLabel);

                _databaseTypeLabel = new Label("Type: DatabaseType");
                _databaseTypeLabel.AddToClassList($"{MainClassName}__type-label");
                headerBlock.Add(_databaseTypeLabel);

                _connectionUriLabel = new Label("connection uri");
                _connectionUriLabel.AddToClassList($"{MainClassName}__uri-label");
                Add(_connectionUriLabel);
            }

            public void Setup(BoltDatabaseInfo databaseInfo)
            {
                _databaseNameLabel.text = databaseInfo.DatabaseName;
                _databaseTypeLabel.text = $"Type: {databaseInfo.DatabaseType}";
                _connectionUriLabel.text = databaseInfo.GetUri();
            }
        }
    }
}
