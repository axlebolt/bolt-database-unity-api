using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorElements
{
    internal class DatabaseEditView : ListView
    {
        public new class UxmlFactory : UxmlFactory<DatabaseEditView, UxmlTraits>
        {}
    }
}
