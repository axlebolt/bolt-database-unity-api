using System;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorElements
{
    internal class ToolbarTabbedView : BindableElement
    {
        public new class UxmlFactory : UxmlFactory<ToolbarTabbedView, UxmlTraits>
        {}

        public Action<ToolbarTabView> NewTabCreatedCallback;
        public Action<ToolbarTabView> TabClosedCallback;
        public Action<ToolbarTabView, bool> TabValueChangedCallback; 

        private readonly VisualElement _tabsContainer;

        private ToolbarTabView _selectedTabView;

        public ToolbarTabbedView()
        {
            _tabsContainer = new VisualElement
            {
                name = "tabs-container",
                style = { flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row) }
            };
            Add(_tabsContainer);

            var newTabButton = new ToolbarButton(OnNewTabButtonClick) { text = "+" };
            Add(newTabButton);
        }

        public void CreateNewTab()
        {
            OnNewTabButtonClick();
        }
        
        private void OnNewTabButtonClick()
        {
            _selectedTabView?.SetValueWithoutNotify(false);
            
            var tabView = new ToolbarTabView() { label = "New Tab" };
            tabView.RegisterValueChangedCallback(evt => OnTabValueChange(tabView, evt));
            tabView.CloseButtonCallback += OnTabClose;
            _tabsContainer.Add(tabView);
            
            NewTabCreatedCallback?.Invoke(tabView);

            tabView.value = true;
        }

        private void OnTabValueChange(ToolbarTabView tabView, ChangeEvent<bool> evt)
        {
            if (evt.newValue)
            {
                _selectedTabView?.SetValueWithoutNotify(false);
                _selectedTabView = tabView;
            }
            else
            {
                if (tabView == _selectedTabView)
                {
                    _selectedTabView.SetValueWithoutNotify(true);
                    return;
                }
                
                Debug.Log("hide window");
            }
            
            TabValueChangedCallback?.Invoke(tabView, evt.newValue);
        }

        private void OnTabClose(ToolbarTabView tabView)
        {
            tabView.RemoveFromHierarchy();
            TabClosedCallback?.Invoke(tabView);

            if (tabView != _selectedTabView)
                return;
            
            _selectedTabView = _tabsContainer.Q<ToolbarTabView>();
            if (_selectedTabView != null)
                _selectedTabView.value = true;
        }
    }
}
