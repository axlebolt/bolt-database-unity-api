using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorElements
{
    internal abstract class PanelViewBase : BindableElement
    {
        protected PanelViewBase()
        {
            AddToClassList("bolt-db-panel-view");
        }
        
        public virtual void Init(AttachToPanelEvent evt)
        {}
        
        public void Show(bool value)
        {
            style.display = new StyleEnum<DisplayStyle>(value ? DisplayStyle.Flex : DisplayStyle.None);

            if (value)
                OnShow();
            else
                OnHide();
        }
        
        protected virtual void OnShow()
        {}
        
        protected virtual void OnHide()
        {}
    }
}
