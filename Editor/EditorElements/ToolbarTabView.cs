using System;
using UnityEditor.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorElements
{
    internal class ToolbarTabView : ToolbarToggle
    {
        public Action<ToolbarTabView> CloseButtonCallback;

        public ToolbarTabView()
        {
            var closeButton = new ToolbarButton(OnCloseButtonClicked) { text = "X" };
            Add(closeButton);
        }

        private void OnCloseButtonClicked() => CloseButtonCallback?.Invoke(this);
    }
}
