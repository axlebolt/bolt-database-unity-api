using System;
using System.Collections.Generic;
using Axlebolt.BoltDatabaseAPI.Data;
using Axlebolt.BoltDatabaseAPI.EditorActions;
using UnityEditor;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI
{
    internal class BoltDatabaseEditorManager
    {
        private static Dictionary<BoltDatabaseType, List<BoltDatabaseEditorActionBase>> _databaseTypeToActions;

        private BoltDatabaseEditor _editor;
        private BoltDatabaseEditorData _data;

        private bool _isInitialized;
        
        public void Init(BoltDatabaseEditor editor)
        {
            _editor = editor;
            Init();
        }

        public List<BoltDatabaseInfo> GetDatabaseInfos()
        {
            CheckDatabaseData();
            return _data.databaseInfos;
        }

        public BoltDatabaseProviderBase ConnectToDatabase(BoltDatabaseInfo databaseInfo)
        {
            return BoltDatabaseManager.Connect(databaseInfo);
        }

        public void Disconnect(BoltDatabaseInfo databaseInfo)
        {
            BoltDatabaseManager.Disconnect(databaseInfo);
        }

        public List<BoltDatabaseEditorActionBase> GetActionsForProvider(BoltDatabaseProviderBase provider)
        {
            return _databaseTypeToActions.TryGetValue(provider.DatabaseType, out var actions) ? actions : null;
        }

        private void Init()
        {
            if (_isInitialized)
                return;
            
            if (_data == null)
                FindOrCreateData();
            
            BoltDatabaseManager.Init();
            FetchActions();

            _isInitialized = true;
        }

        private static void FetchActions()
        {
            if (_databaseTypeToActions != null)
                return;

            _databaseTypeToActions = new Dictionary<BoltDatabaseType, List<BoltDatabaseEditorActionBase>>(new BoltDatabaseTypeEqualityComparer());

            var types = ReflectionHelper.GetTypesDerivedFrom<BoltDatabaseEditorActionBase>();
            foreach (var actionType in types)
            {
                var action = Activator.CreateInstance(actionType) as BoltDatabaseEditorActionBase;

                if (!_databaseTypeToActions.TryGetValue(action!.TargetType, out var actionsCollection))
                {
                    actionsCollection = new List<BoltDatabaseEditorActionBase>();
                    _databaseTypeToActions.Add(action.TargetType, actionsCollection);
                }
                
                actionsCollection.Add(action);
            }
        }

        private void CheckDatabaseData()
        {
            Init();
            
            if (_data == null || _data.databaseInfos == null || _data.databaseInfos.Count == 0)
                throw new NullReferenceException("No Database data provided");
        }

        private void FindOrCreateData()
        {
            const string boltDataAssetName = "BoltDatabaseEditorData";
            const string boltDatabaseFolder = "BoltDatabase";
            var folderPath = $"Assets/{boltDatabaseFolder}";
            var assetPath = $"{folderPath}/{boltDataAssetName}.asset";
            if (!AssetDatabase.IsValidFolder(folderPath))
                AssetDatabase.CreateFolder("Assets", boltDatabaseFolder);

            var asset = AssetDatabase.LoadAssetAtPath<BoltDatabaseEditorData>($"{assetPath}");
            if (asset == null)
            {
                asset = ScriptableObject.CreateInstance<BoltDatabaseEditorData>();
                asset.name = boltDataAssetName;
                AssetDatabase.CreateAsset(asset, assetPath);
                AssetDatabase.SaveAssets();
            }
            
            _data = asset;
        }
    }
}
