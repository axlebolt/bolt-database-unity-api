using Axlebolt.BoltDatabaseAPI.Data;

namespace Axlebolt.BoltDatabaseAPI.EditorActions
{
    public abstract class BoltDatabaseEditorActionBase
    {
        public abstract BoltDatabaseType TargetType { get; }
        public virtual string DisplayName => GetType().Name;

        public abstract void Execute(BoltDatabaseProviderBase provider);
    }
}
