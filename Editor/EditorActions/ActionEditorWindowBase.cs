using System;
using Axlebolt.BoltDatabaseAPI.Data;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorActions
{
    public abstract class ActionEditorWindowBase<T> : EditorWindow
    {
        public Action ApplyCallback;

        protected BoltDataEditorContainer<T> _dataContainer;
        protected SerializedObject _serializedObject;

        protected PropertyField _propertyField;
        
        public void CreateGUI()
        {
            OnBeforeCreatePropertyField();
            
            var root = rootVisualElement;
            _propertyField = new PropertyField();
            root.Add(_propertyField);
            
            OnAfterCreatePropertyField();

            var applyButton = new Button(Apply) { text = "Apply" };
            root.Add(applyButton);
        }
        
        protected virtual void OnBeforeCreatePropertyField()
        {}
        
        protected virtual void OnAfterCreatePropertyField()
        {}
        
        protected virtual void Apply()
        {
            _serializedObject.ApplyModifiedProperties();
            ApplyCallback?.Invoke();
            
            Close();
        }

        public void SetData(T data)
        {
            _dataContainer = CreateContainer();
            _dataContainer.Data = data;
            _serializedObject = new SerializedObject(_dataContainer);
            _propertyField.bindingPath = "Data";
            _propertyField.BindProperty(_serializedObject);
        }

        protected abstract BoltDataEditorContainer<T> CreateContainer();
        
        protected virtual void OnDestroy()
        {
            if (_dataContainer != null)
                DestroyImmediate(_dataContainer);
        }
    }
}
