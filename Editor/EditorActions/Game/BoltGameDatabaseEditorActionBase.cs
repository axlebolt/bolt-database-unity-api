using Axlebolt.BoltDatabaseAPI.Data;

namespace Axlebolt.BoltDatabaseAPI.EditorActions.Game
{
    public abstract class BoltGameDatabaseEditorActionBase : BoltDatabaseEditorActionBase
    {
        public override BoltDatabaseType TargetType => BoltDatabaseType.Game;
    }
}
