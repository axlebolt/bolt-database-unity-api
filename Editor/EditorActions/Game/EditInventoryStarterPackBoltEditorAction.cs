using Axlebolt.BoltDatabaseAPI.Data;
using Axlebolt.BoltDatabaseAPI.Data.Game;
using Axlebolt.BoltDatabaseAPI.Models.Game;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.EditorActions.Game
{
    internal class EditInventoryStarterPackBoltEditorAction : BoltGameDatabaseEditorActionBase
    {
        public override string DisplayName => "Edit Inventory Starter Pack";
        
        private BoltGameDatabaseProvider _provider;
        private BoltInventoryStarterPackDocument _starterPackDocument;
        
        public override void Execute(BoltDatabaseProviderBase provider)
        {
            _provider = provider as BoltGameDatabaseProvider;
            
            var window = EditorWindow.GetWindow<EditInventoryStarterPackActionEditor>();
            window.titleContent = new GUIContent(DisplayName);
            window.ApplyCallback += OnApplyAsync;
            
            ExecuteAsync(window);
        }

        private async void ExecuteAsync(EditInventoryStarterPackActionEditor window)
        {
            _starterPackDocument = await _provider.GetBoltInventoryStarterPackAsync("Getting Inventory Starter Pack");
            window.SetData(_starterPackDocument);
        }

        private async void OnApplyAsync()
        {
            await _provider.SetBoltInventoryStarterPackAsync(_starterPackDocument, "Setting Inventory Starter Pack");
        }
    }
    
    internal class EditInventoryStarterPackActionEditor : ActionEditorWindowBase<BoltInventoryStarterPackDocument>
    {
        private ObjectField _sourceObjectField;

        protected override void OnBeforeCreatePropertyField()
        {
            _sourceObjectField = new ObjectField("From Source Object")
            {
                objectType = typeof(BoltInventoryStarterPackDefinition)
            };
            _sourceObjectField.RegisterValueChangedCallback(OnSourceObjectSet);
            rootVisualElement.Add(_sourceObjectField);
        }

        private void OnSourceObjectSet(ChangeEvent<Object> evt)
        {
            if (evt.newValue == null)
                return;
            
            var sourceObject = evt.newValue as BoltInventoryStarterPackDefinition;
            _dataContainer.Data.UpdateFrom(sourceObject!.Data);
        }

        protected override BoltDataEditorContainer<BoltInventoryStarterPackDocument> CreateContainer()
        {
            return CreateInstance<BoltInventoryStarterPackDefinition>();
        }
    }
}
