using System;
using System.Collections.Generic;
using System.Linq;
using Axlebolt.BoltDatabaseAPI.Data;
using Axlebolt.BoltDatabaseAPI.Models.Game;
using MongoDB.Bson;
using UnityEditor;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.EditorActions.Game
{
    internal class AddInventoryItemDefinitionsBoltEditorAction : BoltGameDatabaseEditorActionBase
    {
        public override string DisplayName => "Add Inventory Item Definitions";
        
        private BoltGameDatabaseProvider _provider;
        private List<BoltInventoryItemDefinitionRequest> _requests;

        public override void Execute(BoltDatabaseProviderBase provider)
        {
            _provider = provider as BoltGameDatabaseProvider;
            
            var window = EditorWindow.GetWindow<AddInventoryItemDefinitionsActionEditor>();
            window.titleContent = new GUIContent(DisplayName);
            window.ApplyCallback = OnAppliedAsync;

            _requests = new List<BoltInventoryItemDefinitionRequest>();
            window.SetData(_requests);
        }

        private async void OnAppliedAsync()
        {
            var mainProcessId = Progress.Start("Creating Inventory Item Definitions", null,
                Progress.Options.Indefinite | Progress.Options.Managed | Progress.Options.Sticky);

            foreach (var definitionRequest in _requests)
            {
                var itemObjectId = await _provider.CreateInventoryItemDefinitionAsync(definitionRequest.GetCreateData(),
                    $"Creating Inventory Item Definition: {definitionRequest.DisplayName}", null, mainProcessId);
                Debug.Log(itemObjectId);
            }

            Progress.Finish(mainProcessId);
        }
    }

    [Serializable]
    internal struct BoltInventoryItemDefinitionRequest
    {
        public string DisplayName;
        public int Key;
        public int GameId;
        public bool Enabled;
        public BoltInventoryItemType ItemType;
        public List<Property> Properties;

        public List<BsonElement> GetProperties()
        {
            if (Properties == null || Properties.Count == 0)
                return null;

            var props = new List<BsonElement>(Properties.Count);
            props.AddRange(Properties.Select(property => property.GetBsonElement()));

            return props;
        }

        public BoltInventoryItemDefinitionCreationData GetCreateData()
        {
            return new BoltInventoryItemDefinitionCreationData
            {
                DisplayName = DisplayName,
                Key = Key,
                CanBeTraded = true,
                Enabled = Enabled,
                ItemType = ItemType.Value,
                GameId = GameId,
                Properties = GetProperties()
            };
        }
        
        [Serializable]
        internal struct Property
        {
            public string Key;
            public BsonType ValueType;
            public string Value;

            public BsonElement GetBsonElement()
            {
                return new BsonElement(Key, GetBsonValue());
            }

            public BsonValue GetBsonValue()
            {
                var value = BsonValue.Create(Value);

                return ValueType switch
                {
                    BsonType.Double => value.ToDouble(),
                    BsonType.String => value.AsString,
                    BsonType.ObjectId => value.AsObjectId,
                    BsonType.Boolean => value.ToBoolean(),
                    BsonType.DateTime => value.AsBsonDateTime,
                    BsonType.Symbol => value.AsBsonSymbol,
                    BsonType.Int32 => value.ToInt32(),
                    BsonType.Timestamp => value.AsBsonTimestamp,
                    BsonType.Int64 => value.ToInt64(),
                    BsonType.Decimal128 => value.ToDecimal(),
                    _ => throw new ArgumentOutOfRangeException()
                };
            }
        }
    }

    internal class BoltInventoryItemRequestContainer : BoltDataEditorContainer<List<BoltInventoryItemDefinitionRequest>>
    {
        private void OnValidate()
        {
            for (var i = 0; i < Data.Count; i++)
            {
                var definitionRequest = Data[i];
                if (definitionRequest.GameId < 1)
                    definitionRequest.GameId = 1;

                if (definitionRequest.Properties != null)
                {
                    foreach (var property in definitionRequest.Properties)
                    {
                        try
                        {
                            property.GetBsonValue();
                        }
                        catch (Exception e)
                        {
                            if (e is ArgumentOutOfRangeException)
                                Debug.LogError($"Value of type {property.ValueType} is not supported");
                        }
                    }
                }

                Data[i] = definitionRequest;
            }
        }
    }

    internal class AddInventoryItemDefinitionsActionEditor : ActionEditorWindowBase<List<BoltInventoryItemDefinitionRequest>>
    {
        protected override BoltDataEditorContainer<List<BoltInventoryItemDefinitionRequest>> CreateContainer()
        {
            return CreateInstance<BoltInventoryItemRequestContainer>();
        }
    }
}
