using System;
using Axlebolt.BoltDatabaseAPI.Data;
using Axlebolt.BoltDatabaseAPI.Models.Main;
using UnityEditor;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.EditorActions.Main
{
    internal sealed class EditGameInfoBoltEditorAction : BoltMainDatabaseEditorActionBase
    {
        public override string DisplayName => "Edit Game Info";

        private BoltMainDatabaseProvider _provider;
        private int _originalDocumentId;

        public override void Execute(BoltDatabaseProviderBase provider)
        {
            _provider = provider as BoltMainDatabaseProvider;
            
            var window = EditorWindow.GetWindow<EditGameInfoActionEditor>();
            window.titleContent = new GUIContent(DisplayName);
            window.ApplyChangesCallback = OnChangesApplied;
            
            ExecuteAsync(window);
        }

        private async void ExecuteAsync(EditGameInfoActionEditor window)
        {
            var document = await _provider.GetGameInfoAsync("Getting GameInfo");
            _originalDocumentId = document._id;
            window.SetData(document);
        }

        private void OnChangesApplied(BoltGameDocument gameDocument)
        {
            _provider.SetGameInfoAsync(gameDocument, _originalDocumentId, "Setting GameInfo");
        }
    }
    
    internal class GameDocumentContainer : BoltDataEditorContainer<BoltGameDocument>
    {}
    
    internal class EditGameInfoActionEditor : ActionEditorWindowBase<BoltGameDocument>
    {
        public Action<BoltGameDocument> ApplyChangesCallback;

        protected override void Apply()
        {
            base.Apply();
            ApplyChangesCallback?.Invoke(_dataContainer.Data);
        }

        protected override BoltDataEditorContainer<BoltGameDocument> CreateContainer()
        {
            return CreateInstance<GameDocumentContainer>();
        }
    }
}
