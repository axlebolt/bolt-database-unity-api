using Axlebolt.BoltDatabaseAPI.Data;

namespace Axlebolt.BoltDatabaseAPI.EditorActions.Main
{
    public abstract class BoltMainDatabaseEditorActionBase : BoltDatabaseEditorActionBase
    {
        public override BoltDatabaseType TargetType => BoltDatabaseType.Main;
    }
}
