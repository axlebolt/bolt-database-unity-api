using System;
using System.Collections.Generic;
using Axlebolt.BoltDatabaseAPI.Data;
using UnityEditor;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.EditorActions.Main
{
    internal sealed class AddTestAccountsBoltEditorAction : BoltMainDatabaseEditorActionBase
    {
        public override string DisplayName => "Add Test Accounts";
        
        private BoltMainDatabaseProvider _provider;
        private List<BoltTestAccountRequest> _requests;

        public override void Execute(BoltDatabaseProviderBase provider)
        {
            _provider = provider as BoltMainDatabaseProvider;
            
            var window = EditorWindow.GetWindow<AddTestAccountsActionEditor>();
            window.titleContent = new GUIContent(DisplayName);
            window.ApplyCallback = OnApplied;

            _requests = new List<BoltTestAccountRequest>();
            window.SetData(_requests);
        }

        private void OnApplied()
        {
            CreateAccountsAsync();
        }

        private async void CreateAccountsAsync()
        {
            var mainProcessId = Progress.Start("Creating players", null,
                Progress.Options.Indefinite | Progress.Options.Managed | Progress.Options.Sticky);
            
            foreach (var accountRequest in _requests)
            {
                if (!accountRequest.Validate())
                    continue;

                await _provider.CreateTestAccountWithPlayerAsync(accountRequest.PlayerName, accountRequest.PlayerId,
                    accountRequest.Token, accountRequest.gameId, "Creating player",
                    $"Creating player: {accountRequest.PlayerName}", mainProcessId);
            }

            Progress.Finish(mainProcessId);
        }
    }

    [Serializable]
    internal struct BoltTestAccountRequest
    {
        public string Token;
        public string PlayerName;
        public string PlayerId;
        public int gameId;

        public bool Validate()
        {
            if (gameId == 0)
                gameId = 1;

            if (string.IsNullOrWhiteSpace(Token))
                return false;

            if (string.IsNullOrWhiteSpace(PlayerName))
                return false;

            return !string.IsNullOrWhiteSpace(PlayerId);
        }
    }
    
    internal class BoltTestAccountRequestsContainer : BoltDataEditorContainer<List<BoltTestAccountRequest>>
    {}
    
    internal class AddTestAccountsActionEditor : ActionEditorWindowBase<List<BoltTestAccountRequest>>
    {
        protected override BoltDataEditorContainer<List<BoltTestAccountRequest>> CreateContainer()
        {
            return CreateInstance<BoltTestAccountRequestsContainer>();
        }
    }
}
