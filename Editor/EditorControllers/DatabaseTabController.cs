using System.Collections.Generic;
using Axlebolt.BoltDatabaseAPI.Data;
using Axlebolt.BoltDatabaseAPI.EditorElements;

namespace Axlebolt.BoltDatabaseAPI.EditorControllers
{
    internal class DatabaseTabController
    {
        private readonly ToolbarTabView _tabView;
        private readonly InspectorView _inspectorView;
        private readonly BoltDatabaseEditorManager _manager;

        private readonly List<BoltDatabaseInfo> _databaseInfos;

        private BoltDatabaseInfo _databaseInfo;
        private BoltDatabaseProviderBase _databaseProvider;

        private DatabaseInfoPanelView _databaseInfoPanel;

        public DatabaseTabController(ToolbarTabView tabView, InspectorView inspectorView, BoltDatabaseEditorManager manager)
        {
            _tabView = tabView;
            _inspectorView = inspectorView;
            _manager = manager;

            _inspectorView.userData = this;
            _databaseInfos = _manager.GetDatabaseInfos();
        }

        public void OnInspectorViewInitialized()
        {
            _inspectorView.OpenNew();

            var newPagePanel = _inspectorView.GetPanel<NewPagePanelView>();
            newPagePanel.SetBindingData(_databaseInfos);
            newPagePanel.DatabaseSelectedCallback += OnDatabaseSelected;
        }

        private void OnDatabaseSelected(BoltDatabaseInfo databaseInfo)
        {
            _databaseInfo = databaseInfo;
            _databaseProvider = _manager.ConnectToDatabase(databaseInfo);
            _databaseProvider.ConnectedCallback += OnConnectedToDatabase;

            _tabView.label = _databaseInfo.DatabaseName;
            _databaseInfoPanel = _inspectorView.OpenPanel<DatabaseInfoPanelView>();
        }

        private void OnConnectedToDatabase()
        {
            _databaseProvider.ConnectedCallback -= OnConnectedToDatabase;
            
            foreach (var databaseName in _databaseProvider.GetFilteredDatabaseNames())
            {
                _databaseInfoPanel.AddDatabase(databaseName, _databaseProvider.GetFilteredCollectionNames(databaseName));
            }
            
            _databaseInfoPanel.SetupActions(_databaseProvider, _manager.GetActionsForProvider(_databaseProvider));
        }
        
        public void Show(bool value)
        {
            _inspectorView.Show(value);
        }

        public void Destroy()
        {
            _inspectorView.RemoveFromHierarchy();
            
            if (_databaseProvider == null)
                return;

            _databaseProvider.ConnectedCallback -= OnConnectedToDatabase;
            _manager.Disconnect(_databaseInfo);
        }
    }
}
