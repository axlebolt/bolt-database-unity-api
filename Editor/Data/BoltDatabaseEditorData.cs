using System.Collections.Generic;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Data
{
    internal class BoltDatabaseEditorData : ScriptableObject
    {
        [SerializeField] public List<BoltDatabaseInfo> databaseInfos;
    }
}
