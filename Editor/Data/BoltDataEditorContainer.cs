using System;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Data
{
    [Serializable]
    public abstract class BoltDataEditorContainer<T> : ScriptableObject
    {
        [SerializeField] public T Data;
    }
}
