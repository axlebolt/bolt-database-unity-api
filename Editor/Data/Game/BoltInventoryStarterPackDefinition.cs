using Axlebolt.BoltDatabaseAPI.Models.Game;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Data.Game
{
    [CreateAssetMenu(menuName = "BOLT/InventoryStarterPackDefinition",
        fileName = "NewBoltInventoryStarterPackDefinition")]
    public class BoltInventoryStarterPackDefinition : BoltDataEditorContainer<BoltInventoryStarterPackDocument>
    {
        private void OnValidate()
        {
            if (Data == null)
                return;

            if (Data.gid < 0)
                Data.gid = 1;
        }
    }
}
