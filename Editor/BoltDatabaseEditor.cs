using System;
using System.Collections.Generic;
using System.Linq;
using Axlebolt.BoltDatabaseAPI.EditorControllers;
using Axlebolt.BoltDatabaseAPI.EditorElements;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI
{
    internal class BoltDatabaseEditor : EditorWindow
    {
        private static readonly BoltDatabaseEditorManager Manager = new();

        #region Elements
        
        private ToolbarTabbedView _tabsView;
        private VisualElement _inspectView;

        #endregion

        #region Templates

        private VisualTreeAsset _inspectorViewAsset;

        #endregion

        private ToolbarTabView _activeTab;
        private readonly Dictionary<ToolbarTabView, DatabaseTabController> _tabToController = new();

        [MenuItem("BOLT/BoltDatabaseEditor")]
        public static void Open()
        {
            var wnd = GetWindow<BoltDatabaseEditor>();
            wnd.titleContent = new GUIContent("BoltDatabaseEditor");
            
            Manager.Init(wnd);
        }

        public void CreateGUI()
        {
            // Each editor window contains a root VisualElement object
            var root = rootVisualElement;
            
            // Import assets
            _inspectorViewAsset = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Packages/com.axlebolt.bolt-db-unity-api/UIToolkitAssets/InspectorView_UXML.uxml");
        
            // Import UXML
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Packages/com.axlebolt.bolt-db-unity-api/UIToolkitAssets/BoltDatabaseEditor_UXML.uxml");
            VisualElement editorUxml = visualTree.Instantiate();
            root.Add(editorUxml.Q("root"));

            // A stylesheet can be added to a VisualElement.
            // The style will be applied to the VisualElement and all of its children.
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.axlebolt.bolt-db-unity-api/UIToolkitAssets/BoltDatabaseEditor_USS.uss");
            root.styleSheets.Add(styleSheet);
            
            FetchElements();
            SetupCallbacks();
            
            _tabsView.CreateNewTab();
        }

        private void FetchElements()
        {
            _tabsView = rootVisualElement.Q<ToolbarTabbedView>();
            _inspectView = rootVisualElement.Q("inspect-view");
        }

        private void SetupCallbacks()
        {
            _tabsView.NewTabCreatedCallback += OnNewTabCreated;
            _tabsView.TabClosedCallback += OnTabClosed;
            _tabsView.TabValueChangedCallback += OnTabValueChanged;
        }

        private void OnNewTabCreated(ToolbarTabView tabView)
        {
            var templateContainer = _inspectorViewAsset.Instantiate();
            var inspector = templateContainer.Q<InspectorView>();
            var controller = new DatabaseTabController(tabView, inspector, Manager);
            controller.Show(false);
            _tabToController.Add(tabView, controller);
            _inspectView.Add(inspector);
        }

        private void OnTabClosed(ToolbarTabView tabView)
        {
            RemoveTab(tabView);
        }

        private void OnTabValueChanged(ToolbarTabView tabView, bool value)
        {
            if (value)
            {
                ShowTab(_activeTab, false);
                _activeTab = tabView;
            }
            
            ShowTab(tabView, value);
        }

        private void ShowTab(ToolbarTabView tabView, bool value)
        {
            if (tabView == null || !_tabToController.TryGetValue(tabView, out var controller))
                return;
            
            controller.Show(value);
        }

        private void RemoveTab(ToolbarTabView tabView)
        {
            if (tabView == null || !_tabToController.TryGetValue(tabView, out var controller))
                return;
            
            controller.Destroy();
            _tabToController.Remove(tabView);
        }

        [MenuItem("BOLT/GetDeviceToken")]
        private static void GetDeviceToken()
        {
            string GetArgsString(string key)
            {
                var argsList = Environment.GetCommandLineArgs().ToList();
                if (argsList.Contains(key) && argsList.FindIndex(x => x == key) < argsList.Count - 2)
                    return argsList[argsList.FindIndex(x => x == key) + 2];
                return "";
            }
            
            var token = GetArgsString("token");
            if (string.IsNullOrEmpty(token))
                token = SystemInfo.deviceUniqueIdentifier;

            GUIUtility.systemCopyBuffer = token;
            Debug.Log($"TOKEN: {token} copied to clipboard");
        }
    }
}