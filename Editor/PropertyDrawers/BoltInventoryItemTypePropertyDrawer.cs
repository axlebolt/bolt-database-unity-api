using System;
using System.Collections.Generic;
using System.Linq;
using Axlebolt.BoltDatabaseAPI.Models.Game;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Axlebolt.BoltDatabaseAPI.PropertyDrawers
{
    // [CustomPropertyDrawer(typeof(BoltInventoryItemType))]
    public class BoltInventoryItemTypePropertyDrawer : PropertyDrawer
    {
        private static Dictionary<int, byte> _displayNameIdToItemTypeId;
        private static List<string> _displayNames;
        private static bool _isInitialized;

        private DropdownField _itemTypesDropdown;
        private SerializedProperty _serializedProperty;

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            Init();

            _serializedProperty = property.FindPropertyRelative(nameof(BoltInventoryItemType.Value));
            var defaultValue = _serializedProperty.intValue;
            _itemTypesDropdown = new DropdownField("BoltInventoryItemType", _displayNames, defaultValue);
            _itemTypesDropdown.RegisterValueChangedCallback(OnValueSelected);

            return _itemTypesDropdown;
        }

        private void OnValueSelected(ChangeEvent<string> evt)
        {
            var selectedItemType = _displayNameIdToItemTypeId[_itemTypesDropdown.index];
            _serializedProperty.intValue = selectedItemType;
            _serializedProperty.serializedObject.ApplyModifiedProperties();
            _serializedProperty.serializedObject.Update();
        }

        private static void Init()
        {
            if (_isInitialized)
                return;

            var authoringTypes = ReflectionHelper.GetTypesDerivedFrom<BoltInventoryItemTypeAuthoringBase>().ToArray();
            _displayNameIdToItemTypeId = new Dictionary<int, byte>(authoringTypes.Length);
            _displayNames = new List<string>(authoringTypes.Length);

            foreach (var authoringType in authoringTypes)
            {
                var authoring = Activator.CreateInstance(authoringType) as BoltInventoryItemTypeAuthoringBase;
                if (_displayNameIdToItemTypeId.ContainsKey(authoring!.ItemTypeId))
                    Debug.LogError($"Duplicate BoltInventoryItemType Authoring for typeId: {authoring.ItemTypeId}");
                
                _displayNames.Add(authoring.ItemTypeDisplayName);
                _displayNameIdToItemTypeId.Add(_displayNames.Count - 1, authoring.ItemTypeId);
            }
            
            _isInitialized = true;
        }
    }
}
