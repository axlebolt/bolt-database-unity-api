using System;
using MongoDB.Bson;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Models.Game
{
    [Serializable]
    public class BoltInventoryItemDefinitionDocument : BoltInventoryItemDocumentBase
    {
        [SerializeField] private string _displayName;
        [SerializeField] private int _itemType;
        [SerializeField] private bool _canBeTraded;
        [SerializeField] private int _key;
        [SerializeField] private BsonDocument _penalty;
        [SerializeField] private BsonArray _buyPrice;
        [SerializeField] private BsonDocument _properties;

        public string displayName
        {
            get => _displayName;
            set => _displayName = value;
        }

        public int itemType
        {
            get => _itemType;
            set => _itemType = value;
        }

        public bool canBeTraded
        {
            get => _canBeTraded;
            set => _canBeTraded = value;
        }
        
        public int key
        {
            get => _key;
            set => _key = value;
        }
        
        public BsonDocument penalty
        {
            get => _penalty;
            set => _penalty = value;
        }

        public BsonArray buyPrice
        {
            get => _buyPrice;
            set => _buyPrice = value;
        }

        public BsonDocument properties
        {
            get => _properties;
            set => _properties = value;
        }
    }
}
