namespace Axlebolt.BoltDatabaseAPI.Models.Game
{
    public class BoltDefaultInventoryItemTypeAuthoring : BoltInventoryItemTypeAuthoringBase
    {
        public override byte ItemTypeId => 0;
        public override string ItemTypeDisplayName => "Default";
    }

    public class BoltTestInventoryItemTypeAuthoring : BoltInventoryItemTypeAuthoringBase
    {
        public override byte ItemTypeId => 1;
        public override string ItemTypeDisplayName => "Test";
    }
}
