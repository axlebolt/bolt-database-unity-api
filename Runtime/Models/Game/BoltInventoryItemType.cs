using System;

namespace Axlebolt.BoltDatabaseAPI.Models.Game
{
    [Serializable]
    public struct BoltInventoryItemType
    {
        public byte Value;
    }
}
