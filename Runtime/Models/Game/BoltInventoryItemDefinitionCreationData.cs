using System.Collections.Generic;
using MongoDB.Bson;

namespace Axlebolt.BoltDatabaseAPI.Models.Game
{
    public struct BoltInventoryItemDefinitionCreationData
    {
        public string DisplayName;
        public int Key;
        public bool CanBeTraded;
        public bool Enabled;
        public int ItemType;
        public int GameId;

        public List<BsonElement> Properties;

        public BsonDocument GetDocument()
        {
            //TODO: rework when ItemType is default property
            Properties ??= new List<BsonElement>(1);
            Properties.Add(new BsonElement("itemType", ItemType));

            return new BsonDocument(new BsonElement[]
            {
                new("displayName", DisplayName),
                new("key", Key),
                new("canBeTraded", CanBeTraded),
                new("enabled", Enabled),
                new("gid", GameId),
                new("properties", new BsonDocument(Properties)),
                new("penalty", new BsonDocument()),
                new("buyPrice", new BsonArray())
            } as IEnumerable<BsonElement>);
        }
    }
}
