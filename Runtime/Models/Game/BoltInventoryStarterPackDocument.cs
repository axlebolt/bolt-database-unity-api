using System;
using System.Collections.Generic;
using MongoDB.Bson;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Models.Game
{
    [Serializable]
    public class BoltInventoryStarterPackDocument : BoltInventoryItemDocumentBase
    {
        [SerializeField] private List<BoltInventoryStarterPackElement> _items;
        
        public List<BoltInventoryStarterPackElement> items
        {
            get => _items;
            set => _items = value;
        }

        public BsonArray currencies;

        public void UpdateFrom(BoltInventoryStarterPackDocument source)
        {
            if (source == null)
                return;

            enabled = source.enabled;
            gid = source.gid;
            
            _items = new List<BoltInventoryStarterPackElement>(source.items);
            if (source.currencies != null)
                currencies = BsonArray.Create(source.currencies.Values);
        }
    }

    [Serializable]
    public class BoltInventoryStarterPackElement
    {
        [SerializeField] private int _itemDefinitionId;
        [SerializeField] private int _value;

        public int itemDefinitionId
        {
            get => _itemDefinitionId;
            set => _itemDefinitionId = value;
        }

        public int value
        {
            get => _value;
            set => _value = value;
        }
    }
}
