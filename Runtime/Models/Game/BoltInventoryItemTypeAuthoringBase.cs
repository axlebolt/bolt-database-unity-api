namespace Axlebolt.BoltDatabaseAPI.Models.Game
{
    public abstract class BoltInventoryItemTypeAuthoringBase
    {
        public abstract byte ItemTypeId { get; }
        public abstract string ItemTypeDisplayName { get; }
    }
}
