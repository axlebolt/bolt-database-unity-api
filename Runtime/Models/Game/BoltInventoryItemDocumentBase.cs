using System;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Models.Game
{
    [Serializable]
    public abstract class BoltInventoryItemDocumentBase : BoltDocumentModelBase
    {
        [SerializeField] private bool _enabled;
        [SerializeField] private int _gameId;
        
        public bool enabled
        {
            get => _enabled;
            set => _enabled = value;
        }
        
        public int gid
        {
            get => _gameId;
            set => _gameId = value;
        }
    }
}
