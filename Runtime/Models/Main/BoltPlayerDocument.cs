using System;
using MongoDB.Bson;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Models.Main
{
    [Serializable]
    public class BoltPlayerDocument : BoltDocumentModelBase
    {
        [SerializeField] private string _uid;
        [SerializeField] private string _name;
        [SerializeField] private string _avatarId;
        [SerializeField] private string _class1;
        [SerializeField] private BsonDateTime _updateDate;
        [SerializeField] private BsonDateTime _createDate;

        public string uid
        {
            get => _uid;
            set => _uid = value;
        }

        public string name
        {
            get => _name;
            set => _name = value;
        }

        public string avatarId
        {
            get => _avatarId;
            set => _avatarId = value;
        }

        public string _class
        {
            get => _class1;
            set => _class1 = value;
        }

        public BsonDateTime updateDate
        {
            get => _updateDate;
            set => _updateDate = value;
        }

        public BsonDateTime createDate
        {
            get => _createDate;
            set => _createDate = value;
        }
    }
}
