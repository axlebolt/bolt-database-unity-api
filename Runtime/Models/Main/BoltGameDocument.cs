using System;
using MongoDB.Bson;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Models.Main
{
    [Serializable]
    public class BoltGameDocument
    {
        [SerializeField] private int _id1;
        [SerializeField] private string _gameUid;
        [SerializeField] private string _code;
        [SerializeField] private BsonDocument _androidSettings;
        [SerializeField] private BsonDocument _iosSettings;
        [SerializeField] private string _minVersion;
        [SerializeField] private string _defaultLocale;

        public int _id
        {
            get => _id1;
            set => _id1 = value;
        }
        
        public string gameUid
        {
            get => _gameUid;
            set => _gameUid = value;
        }

        public string code
        {
            get => _code;
            set => _code = value;
        }

        public BsonDocument androidSettings
        {
            get => _androidSettings;
            set => _androidSettings = value;
        }

        public BsonDocument iosSettings
        {
            get => _iosSettings;
            set => _iosSettings = value;
        }

        public string minVersion
        {
            get => _minVersion;
            set => _minVersion = value;
        }

        public string defaultLocale
        {
            get => _defaultLocale;
            set => _defaultLocale = value;
        }
    }
}
