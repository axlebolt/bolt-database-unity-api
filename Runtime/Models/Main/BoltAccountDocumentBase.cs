using System;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Models.Main
{
    [Serializable]
    public abstract class BoltAccountDocumentBase : BoltDocumentModelBase
    {
        [SerializeField] private string _playerId;
        [SerializeField] private int _gid;

        public string playerId
        {
            get => _playerId;
            set => _playerId = value;
        }

        public int gid
        {
            get => _gid;
            set => _gid = value;
        }
    }
}
