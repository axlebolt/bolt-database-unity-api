using System;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Models.Main
{
    [Serializable]
    public class BoltTestAccountDocument : BoltAccountDocumentBase
    {
        [SerializeField] private string _token;

        public string token
        {
            get => _token;
            set => _token = value;
        }
    }
}
