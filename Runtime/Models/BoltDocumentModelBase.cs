using System;
using MongoDB.Bson;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Models
{
    [Serializable]
    public abstract class BoltDocumentModelBase
    {
        [SerializeField] private ObjectId _id1;

        public ObjectId _id
        {
            get => _id1;
            set => _id1 = value;
        }
    }
}
