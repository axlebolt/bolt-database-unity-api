using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Axlebolt.BoltDatabaseAPI.Data;
using MongoDB.Driver;
using UnityEditor;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI
{
    public abstract class BoltDatabaseProviderBase
    {
        public abstract BoltDatabaseType DatabaseType { get; }

        public Action ConnectedCallback;

        protected BoltDatabaseInfo _databaseInfo;

        protected MongoClient _client;

        protected List<string> _databaseNames;

        protected readonly Dictionary<string, List<string>> _databaseNameToCollectionNames = new();

        public void Connect(BoltDatabaseInfo databaseInfo)
        {
            _databaseInfo = databaseInfo;
            Debug.Log($"Connecting to database {databaseInfo.DatabaseName} | {databaseInfo.GetUri()}");

            _client = new MongoClient(databaseInfo.GetUri());
            ConnectAsync();
        }

        public void Disconnect()
        {
            Debug.Log($"Disconnect from database: {_databaseInfo.DatabaseName} | {_databaseInfo.GetUri()}");
        }

        private async void ConnectAsync()
        {
#if UNITY_EDITOR
            var mainProgressId = Progress.Start("Getting list of BoltDBs",
                            $"Getting list of BoltDBs for: '{_databaseInfo.DatabaseName}'", Progress.Options.Indefinite);
#endif

            _databaseNames = await GetDatabaseNamesAsync();

            await FilterDatabaseNames(_databaseNames);
            
            _databaseNameToCollectionNames.Clear();
            
            foreach (var databaseName in _databaseNames)
            {
                var collectionNames = await Async(() => GetCollectionNames(databaseName), "Getting collections", null
#if UNITY_EDITOR
                    , mainProgressId
#endif
                );
                await FilterCollectionNames(databaseName, collectionNames);
                _databaseNameToCollectionNames.Add(databaseName, collectionNames);
            }

#if UNITY_EDITOR
            Progress.Remove(mainProgressId);
#endif
            
            ConnectedCallback?.Invoke();
        }

        #region Getters

        public List<string> GetFilteredDatabaseNames() => _databaseNames;

        public List<string> GetFilteredCollectionNames(string databaseName) => _databaseNameToCollectionNames[databaseName];

        #endregion

        protected virtual async Task FilterDatabaseNames(List<string> databaseNames)
        {}
        
        protected virtual async Task FilterCollectionNames(string databaseName, List<string> collectionNames)
        {}

        public List<string> GetDatabaseNames()
        {
            var databases = _client.ListDatabaseNames();
            return databases.ToList();
        }

        public Task<List<string>> GetDatabaseNamesAsync()
        {
            return Async(GetDatabaseNames);
        }

        public List<string> GetCollectionNames(string databaseName)
        {
            var database = _client.GetDatabase(databaseName);
            var collections = database.ListCollectionNames();
            return collections.ToList();
        }

        public Task<List<string>> GetCollectionNamesAsync(string databaseName)
        {
            return Async(() => GetCollectionNames(databaseName));
        }

        public static async Task Async(Action action)
        {
            var task = Task.Factory.StartNew(action);
            await task;
        }
        
        public static async Task Async(Action action, string processName, string processDescription = null, int parentProcessId = -1)
        {
#if UNITY_EDITOR
            var processId = Progress.Start(processName, processDescription, Progress.Options.Indefinite | Progress.Options.Managed, parentProcessId);
#endif
            
            await Async(action);

#if UNITY_EDITOR
            Progress.Finish(processId);
#endif
        }

        public static async Task<T> Async<T>(Func<T> func)
        {
            T value = default;
            var task = Task.Factory.StartNew(() =>
            {
                value = func.Invoke();
            });
            await task;

            return value;
        }

        public static async Task<T> Async<T>(Func<T> func, string processName, string processDescription = null, int parentProcessId = -1)
        {
#if UNITY_EDITOR
            var processId = Progress.Start(processName, processDescription, Progress.Options.Indefinite | Progress.Options.Managed, parentProcessId);
#endif
            var value = await Async(func);
            
#if UNITY_EDITOR
            Progress.Finish(processId);
#endif

            return value;
        }
    }
}
