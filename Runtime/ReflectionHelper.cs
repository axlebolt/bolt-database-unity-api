using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;

namespace Axlebolt.BoltDatabaseAPI
{
    public static class ReflectionHelper
    {
        public static IEnumerable<Type> GetTypesDerivedFrom<T>(bool withParent = false, bool withAbstract = false)
        {
            var type = typeof(T);
            var types = new List<Type>();
            
#if UNITY_EDITOR
            var derivedTypes = TypeCache.GetTypesDerivedFrom<T>().ToArray();
            types.AddRange(from t in derivedTypes
                where withParent || type != t 
                where withAbstract || !t.IsAbstract select t);

            return types;
#endif
            
            
#pragma warning disable 162
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    var assemblyTypes = assembly.GetTypes();
                    types.AddRange(from t in assemblyTypes 
                        where type.IsAssignableFrom(t) 
                        where withParent || type != t 
                        where withAbstract || !t.IsAbstract select t);
                }
                catch (ReflectionTypeLoadException e)
                {
                    types.AddRange(from t in e.Types 
                        where t != null && type.IsAssignableFrom(t) 
                        where withParent || type != t 
                        where withAbstract || !t.IsAbstract select t);
                }
            }

            return types;
#pragma warning restore 162
        }
    }
}
