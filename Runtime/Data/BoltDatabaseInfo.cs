using System;
using System.Collections.Generic;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI.Data
{
    [Serializable]
    public struct BoltDatabaseInfo : IEquatable<BoltDatabaseInfo>
    {
        public string DatabaseName;
        public BoltDatabaseType DatabaseType;

        [Header("Address")]
        public string Address;
        public string Port;

        [Header("Auth")]
        public string DatabaseAuth;
        public string UserName;
        public string Password;

        [Header("ConnectionURI")]
        public string Uri;

        public string GetUri()
        {
            return !string.IsNullOrWhiteSpace(Uri) ? Uri : $"mongodb://{UserName}:{Password}@{Address}:{Port}/{UserName}?authSource={DatabaseAuth}";
        }
        
        public bool Equals(BoltDatabaseInfo other)
        {
            return Address == other.Address && Port == other.Port;
        }

        public override int GetHashCode()
        {
            return Address.GetHashCode() + Port.GetHashCode();
        }
    }

    public enum BoltDatabaseType : byte
    {
        Main = 0,
        Game =1
    }

    #region EqualityComparers

    public class BoltDatabaseInfoEqualityComparer : IEqualityComparer<BoltDatabaseInfo>
    {
        public bool Equals(BoltDatabaseInfo x, BoltDatabaseInfo y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(BoltDatabaseInfo obj)
        {
            return obj.GetHashCode();
        }
    }
    
    public class BoltDatabaseTypeEqualityComparer : IEqualityComparer<BoltDatabaseType>
    {
        public bool Equals(BoltDatabaseType x, BoltDatabaseType y)
        {
            return x == y;
        }

        public int GetHashCode(BoltDatabaseType obj)
        {
            return (int)obj;
        }
    }

    #endregion
}
