using System.Collections.Generic;
using System.Threading.Tasks;
using Axlebolt.BoltDatabaseAPI.Data;
using Axlebolt.BoltDatabaseAPI.Models.Game;
using MongoDB.Bson;
using MongoDB.Driver;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI
{
    public class BoltGameDatabaseProvider : BoltDatabaseProviderBase
    {
        public override BoltDatabaseType DatabaseType => BoltDatabaseType.Game;
        
        private const string BoltDatabaseName = "bolt";
        
        protected override Task FilterDatabaseNames(List<string> databaseNames)
        {
            if (databaseNames == null) 
                return base.FilterDatabaseNames(null);
            
            databaseNames.RemoveAll(s => s != "bolt");
            return base.FilterDatabaseNames(databaseNames);
        }
        
        protected override Task FilterCollectionNames(string databaseName, List<string> collectionNames)
        {
            if (collectionNames == null)
                return base.FilterCollectionNames(databaseName, null);

            collectionNames.RemoveAll(s => s.StartsWith("system."));
            
            return base.FilterCollectionNames(databaseName, collectionNames);
        }

        #region Actions
        #region InventoryItemDefinition

        private const string InventoryItemDefinitionCollectionName = "inventory_item_definition";
        
        public BoltInventoryItemDefinitionDocument GetInventoryItemDefinition(int key, int gameId)
        {
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BoltInventoryItemDefinitionDocument>(InventoryItemDefinitionCollectionName);
            var filterBuilder = new FilterDefinitionBuilder<BoltInventoryItemDefinitionDocument>();
            var filter = filterBuilder.Eq("key", key) & filterBuilder.Eq("gid", gameId);
            return collection.Find(filter).FirstOrDefault();
        }

        public Task<BoltInventoryItemDefinitionDocument> GetInventoryItemDefinitionAsync(int key, int gameId, string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(() => GetInventoryItemDefinition(key, gameId), processName, processDescription, parentProcessId);
        }

        public ObjectId CreateInventoryItemDefinition(BoltInventoryItemDefinitionCreationData definitionCreationData)
        {
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BsonDocument>(InventoryItemDefinitionCollectionName);
            collection.InsertOne(definitionCreationData.GetDocument());

            var definitionsCollection = database.GetCollection<BoltInventoryItemDefinitionDocument>(InventoryItemDefinitionCollectionName);
            var filterBuilder = new FilterDefinitionBuilder<BoltInventoryItemDefinitionDocument>();
            var filter = filterBuilder.Eq("key", definitionCreationData.Key) &
                         filterBuilder.Eq("gid", definitionCreationData.GameId);
            var createdItemDefinition = definitionsCollection.Find(filter).First();
            return createdItemDefinition._id;
        }

        public Task<ObjectId> CreateInventoryItemDefinitionAsync(BoltInventoryItemDefinitionCreationData definitionCreationData, string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(() => CreateInventoryItemDefinition(definitionCreationData), processName, processDescription, parentProcessId);
        }

        public void ReplaceInventoryItemDefinition(BoltInventoryItemDefinitionDocument definitionDocument)
        {
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BoltInventoryItemDefinitionDocument>(InventoryItemDefinitionCollectionName);
            var filter = new FilterDefinitionBuilder<BoltInventoryItemDefinitionDocument>().Eq("_id", definitionDocument._id);
            var result = collection.ReplaceOne(filter, definitionDocument);
            Debug.Log(result);
        }

        public Task ReplaceInventoryItemDefinitionAsync(BoltInventoryItemDefinitionDocument definitionDocument, string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(() => ReplaceInventoryItemDefinition(definitionDocument), processName, processDescription, parentProcessId);
        }

        public List<BoltInventoryItemDefinitionDocument> GetAllItemDefinitionsByType(uint inventoryItemTypeId)
        {
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BoltInventoryItemDefinitionDocument>(InventoryItemDefinitionCollectionName);
            FilterDefinition<BoltInventoryItemDefinitionDocument> filter =
                "{" + "properties : {itemType :" + $"{inventoryItemTypeId}" + "}}";
            return collection.Find(filter).ToList();
        }

        public Task<List<BoltInventoryItemDefinitionDocument>> GetAllItemDefinitionsByTypeAsync(uint inventoryItemTypeId, string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(() => GetAllItemDefinitionsByType(inventoryItemTypeId), processName, processDescription, parentProcessId);
        }
        #endregion

        #region InventoryStarterPack

        private const string InventoryStarterPackCollectionName = "inventory_starter_pack";

        public BoltInventoryStarterPackDocument GetBoltInventoryStarterPack()
        {
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BoltInventoryStarterPackDocument>(InventoryStarterPackCollectionName);
            return collection.Find(new FilterDefinitionBuilder<BoltInventoryStarterPackDocument>().Empty).First();
        }

        public Task<BoltInventoryStarterPackDocument> GetBoltInventoryStarterPackAsync()
        {
            return Async(GetBoltInventoryStarterPack);
        }

        public Task<BoltInventoryStarterPackDocument> GetBoltInventoryStarterPackAsync(string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(GetBoltInventoryStarterPack, processName, processDescription, parentProcessId);
        }

        public void SetBoltInventoryStarterPack(BoltInventoryStarterPackDocument starterPackDocument)
        {
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BoltInventoryStarterPackDocument>(InventoryStarterPackCollectionName);
            var filter = new FilterDefinitionBuilder<BoltInventoryStarterPackDocument>().Eq("_id", starterPackDocument._id);
            var result = collection.ReplaceOne(filter, starterPackDocument);
            Debug.Log(result);
        }

        public Task SetBoltInventoryStarterPackAsync(BoltInventoryStarterPackDocument starterPackDocument)
        {
            return Async(() => SetBoltInventoryStarterPack(starterPackDocument));
        }

        public Task SetBoltInventoryStarterPackAsync(BoltInventoryStarterPackDocument starterPackDocument, string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(() => SetBoltInventoryStarterPack(starterPackDocument), processName, processDescription, parentProcessId);
        }

        #endregion
        #endregion
    }
}
