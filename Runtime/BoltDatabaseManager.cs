using System;
using System.Collections.Generic;
using Axlebolt.BoltDatabaseAPI.Data;

namespace Axlebolt.BoltDatabaseAPI
{
    public static class BoltDatabaseManager
    {
        private static readonly Dictionary<BoltDatabaseType, Type> DatabaseTypeToProviderType = new(new BoltDatabaseTypeEqualityComparer());
        private static readonly Dictionary<BoltDatabaseInfo, BoltDatabaseProviderBase> DatabaseInfoToProvider = new(new BoltDatabaseInfoEqualityComparer());

        public static void Init()
        {
            InitProviders();
        }

        public static BoltDatabaseProviderBase Connect(BoltDatabaseInfo databaseInfo)
        {
            if (!DatabaseInfoToProvider.TryGetValue(databaseInfo, out var provider))
            {
                if (!DatabaseTypeToProviderType.TryGetValue(databaseInfo.DatabaseType, out var providerType))
                    throw new Exception($"BoltDatabaseProvider for type {databaseInfo.DatabaseType} is not implemented");
                
                provider = Activator.CreateInstance(providerType) as BoltDatabaseProviderBase;
                DatabaseInfoToProvider.Add(databaseInfo, provider);
            }
            
            provider!.Connect(databaseInfo);

            return provider;
        }

        public static void Disconnect(BoltDatabaseInfo databaseInfo)
        {
            if (!DatabaseInfoToProvider.TryGetValue(databaseInfo, out var provider))
                return;
            
            provider.Disconnect();
            DatabaseInfoToProvider.Remove(databaseInfo);
        }

        private static void InitProviders()
        {
            DatabaseTypeToProviderType.Clear();

            var types = ReflectionHelper.GetTypesDerivedFrom<BoltDatabaseProviderBase>();
            foreach (var providerType in types)
            {
                var provider = Activator.CreateInstance(providerType) as BoltDatabaseProviderBase;
                DatabaseTypeToProviderType.TryAdd(provider!.DatabaseType, providerType);
            }
        }
    }
}
