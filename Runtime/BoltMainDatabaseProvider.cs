using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Axlebolt.BoltDatabaseAPI.Data;
using Axlebolt.BoltDatabaseAPI.Models.Main;
using MongoDB.Bson;
using MongoDB.Driver;
using UnityEngine;

namespace Axlebolt.BoltDatabaseAPI
{
    public class BoltMainDatabaseProvider : BoltDatabaseProviderBase
    {
        public override BoltDatabaseType DatabaseType => BoltDatabaseType.Main;

        private const string BoltDatabaseName = "bolt";

        protected override Task FilterDatabaseNames(List<string> databaseNames)
        {
            if (databaseNames == null) 
                return base.FilterDatabaseNames(null);
            
            databaseNames.RemoveAll(s => s != "bolt");
            return base.FilterDatabaseNames(databaseNames);
        }

        protected override Task FilterCollectionNames(string databaseName, List<string> collectionNames)
        {
            if (collectionNames == null)
                return base.FilterCollectionNames(databaseName, null);

            collectionNames.RemoveAll(s => s.StartsWith("system."));
            
            return base.FilterCollectionNames(databaseName, collectionNames);
        }

        #region Actions
        #region EditGameInfo

        public BoltGameDocument GetGameInfo()
        {
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BoltGameDocument>("game");
            return collection.Find(new FilterDefinitionBuilder<BoltGameDocument>().Empty).First();
        }

        public Task<BoltGameDocument> GetGameInfoAsync()
        {
            return Async(GetGameInfo);
        }
        
        public Task<BoltGameDocument> GetGameInfoAsync(string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(GetGameInfo, processName, processDescription, parentProcessId);
        }

        public void SetGameInfo(BoltGameDocument gameDocument, int originalId)
        {
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BoltGameDocument>("game");
            var filter = new FilterDefinitionBuilder<BoltGameDocument>().Eq("_id", originalId);
            var result = collection.ReplaceOne(filter, gameDocument);
            Debug.Log(result);
        }

        public Task SetGameInfoAsync(BoltGameDocument gameDocument, int originalId)
        {
            return Async(() => SetGameInfo(gameDocument, originalId));
        }

        public Task SetGameInfoAsync(BoltGameDocument gameDocument, int originalId, string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(() => SetGameInfo(gameDocument, originalId), processName, processDescription, parentProcessId);
        }

        #endregion

        #region AddTestAccounts

        public ObjectId CreatePlayer(string playerName, string playerUid)
        {
            const string collectionName = "player";
            
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BsonDocument>(collectionName);
            collection.InsertOne(new BsonDocument(new BsonElement[]
            {
                new("name", playerName), 
                new("uid", playerUid),
                new("createDate", (BsonDateTime)DateTime.Now)
            } as IEnumerable<BsonElement>));

            var playersCollection = database.GetCollection<BoltPlayerDocument>(collectionName);
            var filter = new FilterDefinitionBuilder<BoltPlayerDocument>().Eq("uid", playerUid);
            var createdPlayer = playersCollection.Find(filter).First();
            return createdPlayer._id;
        }

        public Task<ObjectId> CreatePlayerAsync(string playerName, string playerUid)
        {
            return Async(() => CreatePlayer(playerName, playerUid));
        }

        public ObjectId CreateTestAccount(string token, int gameId)
        {
            const string collectionName = "account_test";
            
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BsonDocument>(collectionName);
            collection.InsertOne(new BsonDocument(new BsonElement[]
            {
                new("token", token),
                new("gid", gameId)
            } as IEnumerable<BsonElement>));

            var accountsCollection = database.GetCollection<BoltTestAccountDocument>(collectionName);
            var filter = new FilterDefinitionBuilder<BoltTestAccountDocument>().Eq("token", token);
            var createdAccount = accountsCollection.Find(filter).First();
            return createdAccount._id;
        }

        public Task<ObjectId> CreateTestAccountAsync(string token, int gameId)
        {
            return Async(() => CreateTestAccount(token, gameId));
        }

        public void SetPlayerForTestAccount(ObjectId accountId, string playerId)
        {
            const string collectionName = "account_test";
            
            var database = _client.GetDatabase(BoltDatabaseName);
            var collection = database.GetCollection<BoltTestAccountDocument>(collectionName);
            var filter = new FilterDefinitionBuilder<BoltTestAccountDocument>().Eq("_id", accountId);
            var update = new UpdateDefinitionBuilder<BoltTestAccountDocument>().Set("playerId", playerId);

            collection.UpdateOne(filter, update);
        }

        public Task SetPlayerForTestAccountAsync(ObjectId accountId, string playerId)
        {
            return Async(() => SetPlayerForTestAccount(accountId, playerId));
        }

        public ObjectId CreateTestAccountWithPlayer(string playerName, string playerUid, string token, int gameId)
        {
            var playerObjectId = CreatePlayer(playerName, playerUid);
            var accountObjectId = CreateTestAccount(token, gameId);
            SetPlayerForTestAccount(accountObjectId, playerObjectId.ToString());

            return accountObjectId;
        }

        public Task<ObjectId> CreateTestAccountWithPlayerAsync(string playerName, string playerUid, string token, int gameId, string processName, string processDescription = null, int parentProcessId = -1)
        {
            return Async(() => CreateTestAccountWithPlayer(playerName, playerUid, token, gameId), processName, processDescription, parentProcessId);
        }

        #endregion
        #endregion
    }
}
